local selection = 1

local menu_list = {
    '开始',
    '压大小王',
    '2048',
    '音乐',
    '第一章',
    '角色测试',
    '关于',
    'GUI_TEST',
    'MAP_TEST',
    '退出'
}

local function draw()
    for i = 1,table.getn(menu_list) do
        if i == selection then
            love.graphics.printf("- "..menu_list[i].." -", 0, 150+i*40, 800, "center")
        else
            love.graphics.printf(menu_list[i], 0, 150+i*40, 800, "center")
        end
    end
end

local function menu_click()
    local item=menu_list[selection]
    local s = require('engine/scene')
    if item=='开始' then
        s.switch('map_test')
    elseif item=='音乐' then
        s.switch('music_player')
    elseif item=='2048' then
        s.switch('game2048')
    elseif item=='压大小王' then
        s.switch('game_biglittle')
    elseif item=='关于' then
        s.switch('about')
    elseif item == '第一章' then
        s.switch('chapter1')
    elseif item == 'GUI_TEST' then
        s.switch('gui_test')
    elseif item == 'MAP_TEST' then
        s.switch('map_test')
    elseif item == '角色测试' then
        s.switch('charactor_test')
    elseif item=='退出' then
        love.event.quit()
    end
end

local function keypressed(key)
    if key == 'up' and selection>1 then
        selection = selection-1
    elseif key == 'down' and selection<table.getn(menu_list) then
        selection = selection+1
    elseif key == 'return' then
        menu_click()
    end
end

local x = {}
x.draw = draw
x.keypressed = keypressed
return x
