local img = {}
local state = 0
local function init()
    img['scene'] = love.graphics.newImage( "data/img/chapter1/scene.jpg" )
    lynne_cg_init()
end

local function draw()
    love.graphics.draw(img['scene'], 0, 0)
    lynne_cg_draw(10, 10, 0.2, 0.2)
    logic_chat_draw()
    love.graphics.print("泡面的觉醒", love.window.getWidth()/2 - 50, 100)
end

local dtotal = 0
local function update(dt)
    if dtotal == 0 then
        lynne_audio_bgm("平凡之路")
    elseif dtotal > 15.0 then
        lynne_logic_chat_play(3)
        dtotal = 0
    elseif dtotal > 10 then
        lynne_logic_chat_play(2)
    elseif dtotal > 5 then
        lynne_cg_load("魔法阵1")
        lynne_logic_chat_play(1)
    end 
    dtotal = dtotal + dt
end

local function keypressed(key)
    local function scene_chapter1_quit()
        state = 0
        lynne_cg_release()
        lynne_logic_chat_release()
        lynne_audio_bgm("我的滑板鞋")
        local s = require('engine/scene')
        s.retreat()
    end

    if key == 'return' then
        scene_chapter1_quit()
    end
end

local x ={}
x.init = init
x.draw = draw
x.keypressed = keypressed
x.update = update
return x